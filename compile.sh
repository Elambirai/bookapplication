# $ javac -cp ".:../libraries/gson-2.8.6.jar" BookApplication.java 
#!/bin/sh

THE_CLASSPATH=
PROGRAM_NAME=BookApplication.java
cd src
for i in `ls ../lib/*.jar`
    do
    THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

javac -classpath ".:${THE_CLASSPATH}" $PROGRAM_NAME

if [ $? -eq 0 ]
then
    echo "compile worked!"
fi