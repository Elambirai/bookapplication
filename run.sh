# $ java -cp ".:../lib/gson-2.8.6.jar" BookApplication

THE_CLASSPATH=
CLASS_NAME=BookApplication
cd src
for i in `ls ../lib/*.jar`
    do
    THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

java -classpath ".:${THE_CLASSPATH}" $CLASS_NAME

if [ $? -eq 0 ]
then
    echo "run worked!"
fi