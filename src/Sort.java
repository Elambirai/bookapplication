import java.util.ArrayList;
import java.util.List;
import java.util.*;
public class Sort {
    public static Scanner io = new Scanner(System.in);
    public static void sortById(List<Books> books)
  {
    List<Integer> ids = new ArrayList<Integer>();
    for(int i=0;i<books.size();i++)
    {
      ids.add(books.get(i).id);
    }
    Collections.sort(ids);
    List<Books> newBooks = new ArrayList<Books>();
    for(int i=0;i<books.size();i++)
    {
      for(int j=0;j<ids.size();j++)
      {
        if(books.get(i).id == ids.get(j))
        {
          if(newBooks.contains(books.get(j)))
            continue;
          newBooks.add(books.get(i));
        }
      }
    }
    BookApplication.display(newBooks);
  }
  public static void sortByTitle(List<Books> books)
  {
    List<String> titles = new ArrayList<String>();
    for(int i=0;i<books.size();i++)
    {
      titles.add(books.get(i).title);
    }
    Collections.sort(titles,String.CASE_INSENSITIVE_ORDER);
    List<Books> newBooks = new ArrayList<Books>();
    for(int i=0;i<titles.size();i++)
    {
      for(int j=0;j<books.size();j++)
      {
        if(titles.get(i).equals(books.get(j).title))
        {
          if(newBooks.contains(books.get(j)))
            continue;
          newBooks.add(books.get(j));
        }
      }
    }
    BookApplication.display(newBooks);
  }
  public static void sortByAuthor(List<Books> books)
  {
    List<String> authors = new ArrayList<String>();
    for(int i=0;i<books.size();i++)
    {
      authors.add(books.get(i).author);
    }
    Collections.sort(authors,String.CASE_INSENSITIVE_ORDER);
    List<Books> newBooks = new ArrayList<Books>();
    for(int i=0;i<authors.size();i++)
    {
      for(int j=0;j<books.size();j++)
      {
        if(authors.get(i).equals(books.get(j).author))
        {
          if(newBooks.contains(books.get(j)))
            continue;
          newBooks.add(books.get(j));
        }
      }
    }
    BookApplication.display(newBooks);
  }
  public static void sortByDate(List<Books> books)
  {
    List<String> ids = new ArrayList<String>();
    for(int i=0;i<books.size();i++)
    {
      ids.add(books.get(i).date);
    }
    Collections.sort(ids);
    List<Books> newBooks = new ArrayList<Books>();
    for(int i=0;i<ids.size();i++)
    {
      for(int j=0;j<books.size();j++)
      {
        
      if(ids.get(i)==books.get(j).date)
      {
        newBooks.add(books.get(j));
      }
      }
    }
    BookApplication.display(newBooks);
  }
  public static void sortByPublisher(List<Books> books)
  {
    List<String> publishers = new ArrayList<String>();
    for(int i=0;i<books.size();i++)
    {
      publishers.add(books.get(i).publisher);
    }
    Collections.sort(publishers,String.CASE_INSENSITIVE_ORDER);
    List<Books> newBooks = new ArrayList<Books>();
    for(int i=0;i<publishers.size();i++)
    {
      for(int j=0;j<books.size();j++)
      {
        if(publishers.get(i).equals(books.get(j).publisher))
        {
          if(newBooks.contains(books.get(j)))
            continue;
          newBooks.add(books.get(j));
        }
      }
    }
    BookApplication.display(newBooks);
  }
}