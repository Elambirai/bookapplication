import java.util.*;
import java.util.stream.*;
import com.google.gson.*;
public class DummyDataCreation {
   public static void main( String[] args ) {
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      List list = Stream.of(new Books("Eloquent JavaScript, Second Edition", "Marijn Haverbeke", "2014-12-14", "No Starch Press"),
                            new Books("Learning JavaScript Design Patterns", "Addy Osmani", "2012-07-01", "O'Reilly Media"),
                            new Books("Speaking JavaScript", "Axel Rauschmayer", "2014-02-01", "O'Reilly Media"),
                            new Books("Programming JavaScript Applications", "Eric Elliott", "2014-07-01", "O'Reilly Media"))
                            .collect(Collectors.toList());
      String json = gson.toJson(list); // converts to json
      System.out.println(json);
   }
}
// Person class
class Books {
   private String title, author, date, publisher;
//    private int ;
   public Books(String title, String author, String date, String publisher) {
      this.title = title;
      this.author = author;
      this.date = date;
      this.publisher = publisher;
   }
   public String toString() {
      return "[" + title + " " + author + " " + date + " " +publisher +"]";
   }
}