import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
public class Constants
{
    public static String JSON_PATH = "../json/";
    public static String BOOKS_JSON_PATH = JSON_PATH+"Books.json";
    public static Type listType = new TypeToken<ArrayList<Books>>(){}.getType();
}