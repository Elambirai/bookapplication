import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.FileNotFoundException;
public class Delete {
    public static Scanner io = new Scanner(System.in);
  public static void deleteById(List<Books> books) throws FileNotFoundException
  {
    //  System.out.println(books.size());
    System.out.println("Enter id");
    int id=io.nextInt();
     for(int i=0;i<books.size();i++)
     {
       if(books.get(i).id == id)
       {
         System.out.println("Are You sure to delete this content");
         System.out.println(books.get(i).toString());
         System.out.println("Enter Yes or No");
         String dec = io.next();
         if(dec.equalsIgnoreCase("Yes"))
         {
          books.remove(books.get(i));
         }
       }
     }
     BookApplication.fileWriter(books);
     BookApplication.display(books);
     //write method
  }

  public static void deleteByTitle(List<Books> books) throws FileNotFoundException
  {
    System.out.println("Enter title");
    String title=io.next();
    title+= io.nextLine();
     for(int i=0;i<books.size();i++)
     {
       if(books.get(i).title.equalsIgnoreCase(title))
       {
         System.out.println("Are You sure to delete this content");
         System.out.println(books.get(i).toString());
         System.out.println("Enter Yes or No");
         String dec = io.next();
         if(dec.equalsIgnoreCase("Yes"))
         {
          books.remove(books.get(i));
         }
       }
    }
    BookApplication.fileWriter(books);
    BookApplication.display(books);
  }

  public static void deleteByAuthor(List<Books> books) throws FileNotFoundException
  {
    System.out.println("Enter author name");
    String author=io.next();
    author += io.nextLine(); 
    List<Books> contents = new ArrayList<Books>();
    for(int i=0;i<books.size();i++)
    {
      if(books.get(i).author.equalsIgnoreCase(author))
        contents.add(books.get(i));
    }
    System.out.println("Are You sure to delete this content");
    System.out.println(contents.size());
    for(int i=0;i<contents.size();i++)
    {
    System.out.println(contents.get(i).toString());
    }
    System.out.println("Enter Yes or No");
    String dec = io.next();
    if(dec.equalsIgnoreCase("Yes"))
    {
      for(int i=0;i<contents.size();i++)
      {
        books.remove(contents.get(i));
      }
    }
    BookApplication.fileWriter(books);
    BookApplication.display(books);
  }

  public static void deleteByPublisher(List<Books> books) throws FileNotFoundException
  {
    System.out.println("Enter Publisher name");
    String publisher=io.next();
    publisher+= io.nextLine();
    List<Books> contents = new ArrayList<Books>();
    for(int i=0;i<books.size();i++)
    {
      if(books.get(i).publisher.equalsIgnoreCase(publisher))
        contents.add(books.get(i));
    }
    System.out.println("Are You sure to delete this content");
    System.out.println(contents.size());
    for(int i=0;i<contents.size();i++)
    {
    System.out.println(contents.get(i).toString());
    }
    System.out.println("Enter Yes or No");
    String dec = io.next();
    if(dec.equalsIgnoreCase("Yes"))
    {
      for(int i=0;i<contents.size();i++)
      {
        books.remove(contents.get(i));
      }
    }
    BookApplication.fileWriter(books);
    BookApplication.display(books);
  }
}
