import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import com.google.gson.*;
import com.google.gson.JsonParser;

class BookApplication
{
  public static Scanner io = new Scanner(System.in);
	public static void main(String[] args) throws FileNotFoundException {
      
      String path = Constants.BOOKS_JSON_PATH;
      BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
      List<Books> booksObj = new Gson().fromJson(bufferedReader, Constants.listType);
      int choices;
      System.out.println("Add\t\t-- Press 1");
      System.out.println("Display\t\t-- Press 2");
      System.out.println("Delete\t\t-- Press 3");
      System.out.println("Sort\t\t-- Press 4");
      System.out.println("Search\t\t-- Press 5");
      System.out.println("Enter Your Choice");
      choices = io.nextInt();
      System.out.println(booksObj.size());
      switch(choices)
      {
        case 1 :
              add(booksObj);
              break;
        case 2 :
              display(booksObj);
              break;
        case 3 : 
              delete(booksObj);
              break;
        case 4 : 
              sort(booksObj);
              break;
        case 5:
            search(io);
            break;
      }
  }
  public static void add(List<Books> bookobjs) throws FileNotFoundException
  {
      Books newBook = new Books();
      newBook.id = bookobjs.size()+1;
      System.out.println("Enter Book Details");
      System.out.println("Enter Book Name");
      newBook.title = io.next();
      newBook.title+= io.nextLine();
      System.out.println("Enter Author Name");
      newBook.author = io.next();
      newBook.author+=io.nextLine();
      System.out.println("Enter Date 'dd-mm-yyyy'");
      newBook.date = io.next();
      System.out.println("Enter Publisher Name");
      newBook.publisher = io.next();
      newBook.publisher+=io.nextLine();
      bookobjs.add(newBook);
      fileWriter(bookobjs);
      
  }
  public static void display(List<Books> books)
  {
     for(int i=0;i<books.size();i++)
     {
        System.out.println("id\t\t:"+books.get(i).id);
        System.out.println("Title\t\t:"+books.get(i).title);
        System.out.println("Author\t\t:"+books.get(i).author);
        System.out.println("Date\t\t:"+books.get(i).date);
        System.out.println("Publisher\t:"+books.get(i).publisher);
     }
  }

  public static void delete(List<Books> book) throws FileNotFoundException
  {
      int choice;
      System.out.println("Enter any option to delete any row");
      System.out.println("id \t\t-- Press 1");
      System.out.println("title \t\t-- Press 2");
      System.out.println("Author\t\t-- Press 3");
      System.out.println("Publisher\t-- Press 4");
      System.out.println("Enter Your Choice");
      choice = io.nextInt();
      switch(choice)
      {
        case 1:
            Delete.deleteById(book);
            break;
        case 2:
            Delete.deleteByTitle(book);
            break;
        case 3:
            Delete.deleteByAuthor(book);
            break;
        case 4:
            Delete.deleteByTitle(book);
            break;
        
      }
      
  }

  public static void sort(List<Books> books)
  {
    System.out.println("Id Based Sort\t\t-- Press 1");
    System.out.println("Title Based Sort\t-- Press 2");
    System.out.println("Author Based Sort\t-- Press 3");
    System.out.println("Date Based Sort\t\t-- Press 4");
    System.out.println("Publisher Based Sort\t-- Press 5");
    System.out.println("Enter Your choices");
    int choice = io.nextInt();
    switch(choice)
    {
      case 1:
        Sort.sortById(books);
        break;
      case 2:
        Sort.sortByTitle(books);
        break;
      case 3:
        Sort.sortByAuthor(books);
        break;
      case 4:
        Sort.sortByDate(books);
        break;
      case 5:
        Sort.sortByPublisher(books);
        break;
    }
  }
  public static void search(Scanner io)
  {
   System.out.println("Enter the option to make search");
   Books searchItem = new Books();
   searchItem.id=io.nextInt();
   searchItem.title=io.next();
   searchItem.title+=io.nextLine();
   searchItem.author=io.next();
   searchItem.author+=io.nextLine();
   searchItem.date=io.next();
   searchItem.publisher=io.next();
   searchItem.publisher+=io.nextLine();

  }

  public static void fileWriter(List<Books> books) throws FileNotFoundException
  {
    BufferedWriter bufferedWriter = null;
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String json = gson.toJson(books,Constants.listType);
    System.out.println(json);
    // json = json.replace("\n", "\r\n");
    try{
      bufferedWriter = new BufferedWriter(new FileWriter(Constants.BOOKS_JSON_PATH));
      bufferedWriter.write(json);
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
    finally
    { 
      try{
        if(bufferedWriter!=null)
        bufferedWriter.close();
      }catch(Exception ex){
        System.out.println("Error in closing the BufferedWriter"+ex);
      }
    }
  }
}